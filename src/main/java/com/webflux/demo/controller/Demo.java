package com.webflux.demo.controller;

import com.webflux.demo.model.Role;
import com.webflux.demo.query.BlogEntity;
import com.webflux.demo.query.BlogRepository;
import com.webflux.demo.utils.ROLE.ManageRole;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Copyright (c) GL Finance Plc. All rights reserved. (http://www.gl-f.com/)
 * Author: Oudam Seng (o.seng@gl-f.com) on 9/18/18.
 **/

@Slf4j
@RestController
@RequestMapping("/api/v1/blog")
public class Demo {

  private final ManageRole manageRole;
  private final BlogRepository repository;

  @Inject
  public Demo(ManageRole manageRole, BlogRepository repository) {
    this.manageRole = manageRole;
    this.repository = repository;
  }

  @GetMapping(value = "/index", produces = "application/json")
  public String getTestString() {
    return "{\"textValue\": \"Test String\"}";
  }

  @Secured(Role.ROLE_USER)
  @GetMapping(value = "/secure", produces = "application/json")
  public String getTestStringSecured() {
    return "{\"textValue\": \"Test String from secured resource\"}";
  }

  @Secured(Role.ROLE_DEMO)
  @GetMapping
  public Flux<BlogEntity> index(KeycloakAuthenticationToken token) {
    return repository.findAll();
  }

  @Secured(Role.ROLE_USER)
  @GetMapping(value = "/save")
  public Mono<BlogEntity> createNewBlog() {
    try {
      BlogEntity blogEntity = new BlogEntity();
      blogEntity.setCreatedBy("System");
      blogEntity.setTitle("Test");
      blogEntity.setContent("Test blog");
      blogEntity.setAuthor("Tester");
      return repository.save(blogEntity);
    } catch (Exception e) {
        log.error(e.getMessage());
    }
    return Mono.empty();
  }

  @GetMapping(value = "/logout")
  public String handleLogout(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    request.logout();
    response.sendRedirect("/");
    return "Logout";
  }
}
