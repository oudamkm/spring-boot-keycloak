package com.webflux.demo.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webflux.demo.utils.jackson.JacksonMappingConfiguration;
import org.keycloak.adapters.KeycloakConfigResolver;
import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.adapters.springsecurity.KeycloakConfiguration;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.client.KeycloakClientRequestFactory;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.keycloak.adapters.springsecurity.filter.KeycloakAuthenticationProcessingFilter;
import org.keycloak.adapters.springsecurity.filter.KeycloakPreAuthActionsFilter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.NullAuthenticatedSessionStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;

import javax.inject.Inject;
@KeycloakConfiguration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true, proxyTargetClass = true)
public class SecurityConfiguration extends KeycloakWebSecurityConfigurerAdapter {

  @Bean
  public GrantedAuthoritiesMapper grantedAuthoritiesMapper() {
    SimpleAuthorityMapper mapper = new SimpleAuthorityMapper();
    mapper.setConvertToUpperCase(true);
    return mapper;
  }

  @Override
  protected KeycloakAuthenticationProvider keycloakAuthenticationProvider() {
    final KeycloakAuthenticationProvider provider = super.keycloakAuthenticationProvider();
    provider.setGrantedAuthoritiesMapper(grantedAuthoritiesMapper());
    return provider;
  }

  @Override
  protected void configure(final AuthenticationManagerBuilder auth) throws ExceptionInInitializerError {
    auth.authenticationProvider(keycloakAuthenticationProvider());
  }

  //Can replace the above 3 methods
 /* @Override
  protected void configure(AuthenticationManagerBuilder auth) throws ExceptionInInitializerError {
    KeycloakAuthenticationProvider keycloakAuthenticationProvider = keycloakAuthenticationProvider();
    final SimpleAuthorityMapper grantedAuthoritiesMapper = new SimpleAuthorityMapper();
    grantedAuthoritiesMapper.setConvertToUpperCase(true);
    keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(grantedAuthoritiesMapper);
    auth.authenticationProvider(keycloakAuthenticationProvider);
  }*/

  @Override
  protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
    return new NullAuthenticatedSessionStrategy();
  }

/*  @Bean
  @Override
  protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
    return new RegisterSessionAuthenticationStrategy(buildSessionRegistry());
  }*/



  @Override
  protected void configure(HttpSecurity http) throws Exception {
    /*super.configure(http);
    http.httpBasic().disable();
    http.formLogin().disable();
    http.anonymous().disable();
    http.csrf().disable();
    http.headers()
      .frameOptions().sameOrigin().and()
      .authorizeRequests()
      .antMatchers("/vaadinServlet/UIDL/**").permitAll()
      .antMatchers("/vaadinServlet/HEARTBEAT/**").permitAll()
      .antMatchers("/h2-console/**").permitAll()
      .antMatchers("/api/**").authenticated()
      .anyRequest().authenticated();*/
    super.configure(http);

    http.authorizeRequests();
  }



  @Bean
  KeycloakConfigResolver keycloakConfigResolver() {
    return new KeycloakSpringBootConfigResolver();
  }

  @SuppressWarnings("unchecked")
  @Bean
  public FilterRegistrationBean keycloakAuthenticationProcessingFilterRegistrationBean(final KeycloakAuthenticationProcessingFilter filter) {
    final FilterRegistrationBean registrationBean = new FilterRegistrationBean(filter);
    registrationBean.setEnabled(false);
    return registrationBean;
  }

  @SuppressWarnings("unchecked")
  @Bean
  public FilterRegistrationBean keycloakPreAuthActionsFilterRegistrationBean(final KeycloakPreAuthActionsFilter filter) {
    final FilterRegistrationBean registrationBean = new FilterRegistrationBean(filter);
    registrationBean.setEnabled(false);
    return registrationBean;
  }

  @Bean
  AuthenticationSuccessNavigationHandler authenticationSuccessHandler() {
    return new AuthenticationSuccessNavigationHandler();
  }

  @Override
  @Bean
  protected KeycloakAuthenticationProcessingFilter keycloakAuthenticationProcessingFilter()
    throws Exception {
    KeycloakAuthenticationProcessingFilter filter = super.keycloakAuthenticationProcessingFilter();
    filter.setAuthenticationSuccessHandler(authenticationSuccessHandler());
    return filter;
  }

/*  @Override
  public void configure(WebSecurity web) throws Exception {
    web.ignoring().antMatchers("/VAADIN/**");
  }*/





  @Bean
  protected SessionRegistry buildSessionRegistry() {
    return new SessionRegistryImpl();
  }






  @Bean
  @Inject
  @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
  public KeycloakRestTemplate keycloakRestTemplate(KeycloakClientRequestFactory keycloakClientRequestFactory) {
    final KeycloakRestTemplate keycloakRestTemplate = new KeycloakRestTemplate(
      keycloakClientRequestFactory);
    keycloakRestTemplate.getMessageConverters().add(0, mappingJacksonHttpMessageConverter());
    return keycloakRestTemplate;
  }


  @Bean
  public MappingJackson2HttpMessageConverter mappingJacksonHttpMessageConverter() {
    MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
    converter.setObjectMapper(objectMapper());
    return converter;
  }


  @Bean
  public ObjectMapper objectMapper() {
    return JacksonMappingConfiguration.createDefaultMapper();
  }

}
