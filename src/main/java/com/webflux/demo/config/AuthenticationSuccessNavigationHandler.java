package com.webflux.demo.config;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class AuthenticationSuccessNavigationHandler implements org.springframework.security.web.authentication.AuthenticationSuccessHandler, org.springframework.security.web.authentication.AuthenticationFailureHandler {

  @Override
  public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException {
    String viewName = httpServletRequest.getRequestURI();
    if (viewName == null)
      return;
    httpServletResponse.sendRedirect("/api/v1/blog");
  }

  @Override
  public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
    httpServletResponse.sendRedirect("/api/v1/blog");
  }
}
