package com.webflux.demo.utils.ROLE;

import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;

/**
 * Copyright (c) GL Finance Plc. All rights reserved. (http://www.gl-f.com/)
 * Author: Oudam Seng (o.seng@gl-f.com) on 9/21/18.
 **/

public interface ManageRole  {
  boolean hasAuthority(KeycloakAuthenticationToken token, String val);
}
