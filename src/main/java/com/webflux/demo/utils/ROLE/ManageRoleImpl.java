package com.webflux.demo.utils.ROLE;

import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

/**
 * Copyright (c) GL Finance Plc. All rights reserved. (http://www.gl-f.com/)
 * Author: Oudam Seng (o.seng@gl-f.com) on 9/21/18.
 **/

@Component
public class ManageRoleImpl implements ManageRole {
  @Override
  public boolean hasAuthority(KeycloakAuthenticationToken token, String val) {
    boolean result = false;
    for(GrantedAuthority grantedAuthority: token.getAuthorities()) {
      if(grantedAuthority.getAuthority().equals(val)) {
        result = grantedAuthority.getAuthority().equals(val);
      }
    }
    return result;
  }
}
