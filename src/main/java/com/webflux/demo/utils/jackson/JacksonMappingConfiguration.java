package com.webflux.demo.utils.jackson;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class JacksonMappingConfiguration {
  private JacksonMappingConfiguration(){}
  public static ObjectMapper createDefaultMapper() {
    final ObjectMapper result = new ObjectMapper();
    result.enable(SerializationFeature.INDENT_OUTPUT);
    result.registerModule(new JavaTimeModule());
    result.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    result.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    return result;
  }
}