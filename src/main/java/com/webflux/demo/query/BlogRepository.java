package com.webflux.demo.query;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Copyright (c) GL Finance Plc. All rights reserved. (http://www.gl-f.com/)
 * Author: Oudam Seng (o.seng@gl-f.com) on 9/22/18.
 **/
@Repository
public interface BlogRepository extends ReactiveCrudRepository<BlogEntity, String> {

  Flux<BlogEntity> findByAuthor(String author);

  Flux<BlogEntity> findByAuthorAndDeleteIsFalse(String author);

  Mono<BlogEntity> findByTitle(String title);

  Mono<BlogEntity> findByIdAndDeleteIsFalse(String id);
}
