package com.webflux.demo.query;

import com.webflux.demo.model.common.BaseEntity;
import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Copyright (c) GL Finance Plc. All rights reserved. (http://www.gl-f.com/)
 * Author: Oudam Seng (o.seng@gl-f.com) on 9/22/18.
 **/

@Data
@Document
public class BlogEntity extends BaseEntity {

  @TextIndexed
  private String title;

  private String content;

  @Indexed
  private String author;
}