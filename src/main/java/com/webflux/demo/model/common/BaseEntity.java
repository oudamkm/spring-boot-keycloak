package com.webflux.demo.model.common;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Copyright (c) GL Finance Plc. All rights reserved. (http://www.gl-f.com/)
 * Author: Oudam Seng (o.seng@gl-f.com) on 9/22/18.
 **/

@Setter
@Getter
public class BaseEntity implements Serializable {

  @Id
  private String id;

  @CreatedBy
  private String createdBy;

  @CreatedDate
  private LocalDateTime createdDate = LocalDateTime.now();

  @LastModifiedBy
  private String updatedBy;

  @LastModifiedDate
  private LocalDateTime updatedDate;

  @Version
  private Long version;

  private Boolean delete = Boolean.FALSE;
}