package com.webflux.demo.model;

/**
 * Copyright (c) GL Finance Plc. All rights reserved. (http://www.gl-f.com/)
 * Author: Oudam Seng (o.seng@gl-f.com) on 9/21/18.
 **/
public class Role {
  public static final String ROLE_DEMO = "ROLE_DEMO";
  public static final String ROLE_USER = "ROLE_USER";
}
